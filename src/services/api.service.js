import axios from 'axios';
import Configuration from '../configProvider';

const API_PVM_URL = `${Configuration.getValue('apiUrl')}`;

export default() => axios.create({
  baseURL: API_PVM_URL,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    charset: 'UTF-8',
  },
});
