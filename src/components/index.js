import Datepicker from 'vuejs-datepicker';
import Vue from 'vue';
import './timeline';
import './inbox';

import App from './App';
import Activity from './Activity';
import DataValidator from './DataValidator';
import DatetimeInput from './DatetimeInput';
import DoqerCreate from './doqer/Create';
import DoqerSelect from './doqer/Select';
import DoqerSuggest from './doqer/Suggest';
import FormInstance from './FormInstance';
import FormRender from './FormRender';
import Header from './Header';
import LinearSteps from './LinearSteps';
import TimeInput from './TimeInput';
import ValueRender from './ValueRender';
import Hero from './Hero';
import SignIn from './SignIn';
import LinkInput from './LinkInput';
import UserInput from './UserInput';
import ArrayInput from './ArrayInput';

import UserCard from './UserCard';
import UserAddCard from './UserAddCard';
import UserCardTabs from './UserCardTabs';

import GroupCard from './GroupCard';
import GroupAddCard from './GroupAddCard';
import GroupList from './GroupList';

import ExecutionCard from './ExecutionCard';
import ExecutionCardTabs from './ExecutionCardTabs';
import ExecutionSummaryCard from './ExecutionSummaryCard';

import PermissionCard from './PermissionCard';
import PermissionList from './PermissionList';

import TaskCard from './TaskCard';
import TaskList from './TaskList';

import UsersPopover from './misc/UsersPopover';
import MdRender from './misc/MdRender';

Vue.component('app-users-popover', UsersPopover);
Vue.component('app-md-render', MdRender);

// Components
Vue.component('app', App);
Vue.component('app-header', Header);
Vue.component('activity', Activity);
Vue.component('form-render', FormRender);
Vue.component('linear-steps', LinearSteps);
Vue.component('value-render', ValueRender);
Vue.component('hero', Hero);
Vue.component('sign-in', SignIn);

// From inputs
Vue.component('form-instance', FormInstance);
Vue.component('time-input', TimeInput);
Vue.component('date-input', Datepicker);
Vue.component('datetime-input', DatetimeInput);
Vue.component('link-input', LinkInput);
Vue.component('user-input', UserInput);
Vue.component('array-input', ArrayInput);

Vue.component('user-card', UserCard);
Vue.component('user-add-card', UserAddCard);
Vue.component('user-card-tabs', UserCardTabs);
Vue.component('group-card', GroupCard);
Vue.component('group-add-card', GroupAddCard);
Vue.component('group-list', GroupList);
Vue.component('execution-card', ExecutionCard);
Vue.component('execution-card-tabs', ExecutionCardTabs);
Vue.component('execution-summary-card', ExecutionSummaryCard);
Vue.component('permission-card', PermissionCard);
Vue.component('permission-list', PermissionList);
Vue.component('task-card', TaskCard);
Vue.component('task-list', TaskList);

// Doqer components
Vue.component('doqer-input', DoqerSelect);
Vue.component('doqer-create', DoqerCreate);
Vue.component('doqer-suggest', DoqerSuggest);

// Data validator
Vue.component('data-validator', DataValidator);
