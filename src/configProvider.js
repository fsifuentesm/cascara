import dotenv from 'dotenv';

dotenv.config();

function actualConfig() {
  return {
    apiUrl: '$VUE_APP_CACAHUATE_URL',
    apiAuths: '$VUE_APP_CACAHUATE_AUTHS',
    doqerUrl: '$VUE_APP_DOQER_URL',
    locale: '$VUE_APP_LOCALE',
    sseEnabled: '$VUE_APP_SSE_ENABLED',
  };
}

function getValue(name) {
  if (!(name in actualConfig())) {
    return null;
  }

  const value = actualConfig()[name];

  if (!value) {
    return null;
  }

  if (value.startsWith('$VUE_APP_')) {
    const envName = value.substr(1);
    const envValue = process.env[envName];
    if (typeof envValue !== 'undefined') {
      return envValue;
    }
  }

  return value;
}


export const ConfigProvider = {
  getValue,
};

export default ConfigProvider;
