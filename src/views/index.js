import Vue from 'vue';

import Inbox from './Inbox';
import Processes from './Processes';
import UserProfile from './UserProfile';
import Users from './Users';
import GroupProfile from './GroupProfile';
import Groups from './Groups';
import ExecutionProfile from './ExecutionProfile';
import Executions from './Executions';

Vue.component('app-inbox', Inbox);
Vue.component('app-processes', Processes);
Vue.component('app-user-profile', UserProfile);
Vue.component('app-users', Users);
Vue.component('app-group-profile', GroupProfile);
Vue.component('app-groups', Groups);
Vue.component('app-execution-profile', ExecutionProfile);
Vue.component('app-executions', Executions);
