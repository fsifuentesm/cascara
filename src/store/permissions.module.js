import Vue from 'vue';

const permissionsState = {
  allItems: {},
  customArrays: {},
};

const actions = {
  addArray({ commit }, payload) {
    commit('addArraySuccess', payload);
  },
};

const mutations = {
  addArraySuccess(ste, payload) {
    Vue.set(ste.customArrays, payload.identifier, {
      data: [],
      query: payload.query,
    });
  },

  getArrayItemsSuccess(ste, payload) {
    Vue.set(ste.customArrays, payload.identifier, {
      ...ste.customArrays[payload.identifier],
      data: payload.items,
    });
  },

  getPermissionsSuccess(ste, items) {
    items.forEach((ptr) => {
      Vue.set(ste.allItems, ptr.identifier, {
        data: ptr,
        loading: false,
        errors: false,
      });
    });
  },
};

export const permissions = {
  namespaced: true,
  state: permissionsState,
  actions,
  mutations,
};

export default permissions;
