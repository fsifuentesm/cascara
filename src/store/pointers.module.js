import Vue from 'vue';
import { pointerService } from '../services/pointer.service';

const pointersState = {
  allItems: {},
  customArrays: {},
};

const actions = {
  addArray({ commit }, payload) {
    commit('addArraySuccess', payload);
  },

  getArrayItems({ state, commit }, identifier) {
    if (state.customArrays[identifier]) {
      const payload = state.customArrays[identifier].query;

      pointerService.getPointers(payload)
        .then(({ items }) => {
          commit(
            'getArrayItemsSuccess',
            {
              identifier,
              items: items.map(x => x.id),
            },
          );
          commit(
            'getPointersSuccess',
            items.map(x => ({
              identifier: x.id,
              name: x.node.name,
              description: x.node.description,
              status: x.state,
              startedAt: x.started_at,
              finishedAt: x.finished_at,
            })),
          );
        });
    }
  },

  getPointer({ commit }, identifier) {
    pointerService.getPointer(identifier)
      .then((pointer) => {
        commit('getPointerSuccess', {
          identifier,
          pointer: {
            identifier: pointer.id,
            name: pointer.node.name,
            description: pointer.node.description,
            status: pointer.state,
            startedAt: pointer.started_at,
            finishedAt: pointer.finished_at,
          },
        });
      });
  },
};

const mutations = {
  addArraySuccess(ste, payload) {
    Vue.set(ste.customArrays, payload.identifier, {
      data: [],
      query: payload.query,
    });
  },

  getArrayItemsSuccess(ste, payload) {
    Vue.set(ste.customArrays, payload.identifier, {
      ...ste.customArrays[payload.identifier],
      data: payload.items,
    });
  },

  getPointersSuccess(ste, items) {
    items.forEach((ptr) => {
      Vue.set(ste.allItems, ptr.identifier, {
        data: ptr,
        loading: false,
        errors: false,
      });
    });
  },

  getPointerSuccess(ste, payload) {
    Vue.set(ste.allItems, payload.identifier, {
      data: payload.pointer,
      loading: false,
      errors: false,
    });
  },
};

export const pointers = {
  namespaced: true,
  state: pointersState,
  actions,
  mutations,
};

export default pointers;
