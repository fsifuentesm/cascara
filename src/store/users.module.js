import Vue from 'vue';
import { userService } from '../services/user.service';

const state = {
  allItems: {},
};

const actions = {
  getAllUsers({ commit }) {
    userService.getUsers()
      .then((items) => {
        commit('getAllUsersSuccess', items);
      });
  },

  getUser({ commit }, identifier) {
    userService.getUser(identifier)
      .then((user) => {
        commit('getUserSuccess', {
          identifier,
          user,
        });
      });
  },

  postUser({ dispatch }, payload) {
    userService.postUser(payload)
      .then((identifier) => {
        dispatch('getUser', identifier);
      });
  },

  getUserGroups({ commit }, identifier) {
    userService.getUserGroups(identifier)
      .then((groups) => {
        commit(
          'groups/getAllGroupsSuccess',
          groups.map(x => ({
            identifier: x.codename,
            name: x.name,
          })),
          { root: true },
        );
        commit(
          'groups/getArrayItemsSuccess',
          {
            identifier: `usr${identifier}`,
            items: groups.map(x => x.codename),
          },
          { root: true },
        );
      });
  },

  getUserPermissions({ commit }, identifier) {
    userService.getUserPermissions(identifier)
      .then((permissions) => {
        commit(
          'permissions/getPermissionsSuccess',
          permissions.map(x => ({
            identifier: x.codename,
            name: x.name,
          })),
          { root: true },
        );
        commit(
          'permissions/getArrayItemsSuccess',
          {
            identifier: `usr${identifier}`,
            items: permissions.map(x => x.codename),
          },
          { root: true },
        );
      });
  },
};

const mutations = {
  getAllUsersSuccess(ste, items) {
    items.forEach((x) => {
      Vue.set(ste.allItems, x.identifier, {
        data: {
          identifier: x.identifier,
          email: x.email,
          fullname: x.fullname,
        },
        loading: false,
        errors: false,
      });
    });
  },

  getUserSuccess(ste, payload) {
    Vue.set(ste.allItems, payload.identifier, {
      data: {
        identifier: payload.user.identifier,
        email: payload.user.email,
        fullname: payload.user.fullname,
      },
      loading: false,
      errors: false,
    });
  },
};

export const users = {
  namespaced: true,
  state,
  actions,
  mutations,
};

export default users;
