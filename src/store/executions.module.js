import Vue from 'vue';
import { executionService } from '../services/execution.service';

const state = {
  allItems: {},
};

const actions = {
  getAllExecutions({ commit }) {
    executionService.getExecutions()
      .then(({ items }) => {
        commit('getAllExecutionsSuccess', items);
      });
  },

  getExecution({ commit }, identifier) {
    executionService.getExecution(identifier)
      .then((execution) => {
        commit('getExecutionSuccess', {
          identifier,
          execution,
        });
      });
  },

  getExecutionSummary({ commit }, identifier) {
    executionService.getExecutionSummary(identifier)
      .then((rawHtml) => {
        commit('getExecutionSummarySuccess', {
          identifier,
          summary: rawHtml,
        });
      });
  },
};

const mutations = {
  getAllExecutionsSuccess(ste, items) {
    items.forEach((x) => {
      Vue.set(ste.allItems, x.id, {
        data: {
          identifier: x.id,
          name: x.name,
          startedAt: x.started_at,
          status: x.status,
        },
        loading: false,
        errors: false,
      });
    });
  },

  getExecutionSuccess(ste, payload) {
    Vue.set(ste.allItems, payload.identifier, {
      data: {
        identifier: payload.execution.id,
        name: payload.execution.name,
        startedAt: payload.execution.started_at,
        status: payload.execution.status,
      },
      loading: false,
      errors: false,
    });
  },

  getExecutionSummarySuccess(ste, payload) {
    Vue.set(ste.allItems, payload.identifier, {
      ...(ste.allItems[payload.identifier] || {}),
      summary: payload.summary,
      loading: false,
      errors: false,
    });
  },
};

export const executions = {
  namespaced: true,
  state,
  actions,
  mutations,
};

export default executions;
