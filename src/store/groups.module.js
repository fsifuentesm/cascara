import Vue from 'vue';
import { groupService } from '../services/group.service';

const state = {
  allItems: {},
  customArrays: {},
};

const actions = {
  addArray({ commit }, payload) {
    commit('addArraySuccess', payload);
  },

  getAllGroups({ commit }) {
    groupService.getGroups()
      .then((items) => {
        commit(
          'getAllGroupsSuccess',
          items.map(x => ({ identifier: x.codename, name: x.name })),
        );
      });
  },

  getGroup({ commit }, identifier) {
    groupService.getGroup(identifier)
      .then((group) => {
        commit('getGroupSuccess', {
          identifier,
          group,
        });
      });
  },

  postGroup({ dispatch }, payload) {
    groupService.postGroup(payload)
      .then((identifier) => {
        dispatch('getGroup', identifier);
      });
  },

  getGroupUsers({ commit }, identifier) {
    groupService.getGroupUsers(identifier)
      .then((users) => {
        commit('getGroupUsersSuccess', {
          identifier,
          users: users.map(x => x.identifier),
        });
        commit(
          'users/getAllUsersSuccess',
          users.map(x => ({
            identifier: x.identifier,
            fullname: x.fullname,
          })),
          { root: true },
        );
      });
  },
};

const mutations = {
  addArraySuccess(ste, payload) {
    Vue.set(ste.customArrays, payload.identifier, {
      data: [],
      query: payload.query,
    });
  },

  getArrayItemsSuccess(ste, payload) {
    Vue.set(ste.customArrays, payload.identifier, {
      ...ste.customArrays[payload.identifier],
      data: payload.items,
    });
  },

  getAllGroupsSuccess(ste, items) {
    items.forEach((grp) => {
      Vue.set(ste.allItems, grp.identifier, {
        data: grp,
        loading: false,
        errors: false,
      });
    });
  },

  getGroupSuccess(ste, payload) {
    Vue.set(ste.allItems, payload.identifier, {
      data: {
        identifier: payload.group.identifier,
        name: payload.group.name,
      },
      loading: false,
      errors: false,
    });
  },

  getGroupUsersSuccess(ste, payload) {
    if (ste.allItems[payload.identifier]) {
      Vue.set(ste.allItems, payload.identifier, Object.assign(
        {
          ...ste.allItems[payload.identifier],
          data: {
            ...ste.allItems[payload.identifier].data,
            users: payload.users,
          },
          loading: false,
          errors: false,
        },
      ));
    } else {
      Vue.set(ste.allItems, payload.identifier, {
        data: {
          users: payload.users,
          identifier: payload.identifier,
        },
        loading: false,
        errors: false,
      });
    }
  },
};

export const groups = {
  namespaced: true,
  state,
  actions,
  mutations,
};

export default groups;
