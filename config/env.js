'use strict'

const envVars = [{
  name: 'VUE_APP_CACAHUATE_URL',
  default: 'http://localhost:5000',
}, {
  name: 'VUE_APP_DOQER_URL',
  default: 'http://localhost:6000',
}, {
  name: 'VUE_APP_CACAHUATE_AUTHS',
  default: [{
    value: 'ldap',
    label: 'LDAP',
  }],
}, {
  name: 'VUE_APP_LOCALE',
  default: 'es',
}, {
  name: 'VUE_APP_SSE_ENABLED',
  default: false,
}];

const base = {};

envVars.forEach((envVar) => {
  let value = JSON.stringify(envVar.default);
  if (process.env[envVar.name] !== undefined) {
    value = process.env[envVar.name];
  }

  base[envVar.name] = value;
});

module.exports = base;
